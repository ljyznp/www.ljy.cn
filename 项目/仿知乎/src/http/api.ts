// 引入二次封装的 axios
import app from './request'

// 登录模块
interface loginType {
    email: string,
    password: string | number
}
export function enterLogin(option: loginType):any {
    let res = app.post('/user/login', option)
    return res
}
// 获取用户信息
export function getUserInfo():any {
    let res = app.get('/user/current')
    return res
}
// columns?currentPage={page}&pageSize={size}
interface Column{
    page:string,
    size:string
}
// 获取专栏
export function getIndex(pages:Column):any {
    let res = app.get(`/columns?currentPage=${pages.page}&pageSize=${pages.size}`)
    return res
}

interface ID{
    id:string,
}

// 获取专栏 详情
export function getColumn(List:ID):any {
    let res = app.get(`/columns/${List.id}`)
    return res
}
interface SpecialConfig {
    page: number,
    pageSize: number,
    author: string
  }
  export const getSpecialList = (options: SpecialConfig) => {
    return app({
      method: 'get',
      url: `columns/${options.author}/posts?currentPage=${options.page}&pageSize=${options.pageSize}`,
    })
  }
  
// 获取文章  posts/{id}

export const getatrcle = (id: ID) => {
    return app({
      method: 'get',
      url: `posts/${id}`,
    })
  }
  
  //填加内容-http://api.vikingship.xyz/api/posts
  interface AddarticleConfig {
    title: string,
    content: string,
    image?: string,//可有可无
    author: string,
    column: string
  }
  export const addArticle = (options: AddarticleConfig) => app({ method: 'post', url: 'posts', data: options })