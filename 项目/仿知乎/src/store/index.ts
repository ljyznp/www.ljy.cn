import { createStore } from 'vuex'
// import persist from 'vuex-persistedstate'
export default createStore({
  state: {
    token: sessionStorage.getItem('token'),
    userInfo: {
      _id: "string",
      email: "string",
      nickName: "string",
    },
    nickName: localStorage.getItem('nickName'),
    // userShow: false,
    enterShow:false||sessionStorage.getItem('enterShow'),
    list:{}
  },
  mutations: {
    settoken(state:any, val:string):void {
      state.token = val
      sessionStorage.setItem('token', state.token)
    },
    setUserinfo(state:any, info:any):void {
      state.userInfo = info
      state.nickName = info.nickName
      console.log(state.nickName);


    },
    // changeUsershow(state):void {
    //   state.userShow = !state.userShow
    //   console.log(state.userShow);

    // },
    changEnter(state,val):void{
      state.enterShow =val
      console.log(state.enterShow);
      sessionStorage.setItem('enterShow',val)
      
    },
  },
  actions: {
  },
  modules: {
  },
  // plugins: [persist()]
})
