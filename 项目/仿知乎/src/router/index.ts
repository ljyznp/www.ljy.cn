import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store/index'
const routes: Array<RouteRecordRaw> = [
  {
    // 一级路由 坑存放的页面
    path: '/',
    name: 'Home',
    component: Home,
    redirect:'/login',
    children:[
      {
        // 首页
        path: '/login',
        name: 'login',
        component: () => import('../views/login.vue')
      },
      {
        // 首页
        path: '/index',
        name: 'index',
        component: () => import('../views/index.vue')
      },
      {
        // 首页
        path: '/avter',
        name: 'avter',
        component: () => import('../views/avter.vue')
      },
      {
        path: '/list',
        name: 'list',
        component: () => import('../views/avterDetail.vue')
      },
      {
        path: '/myList',
        name: 'myList',
        component: () => import('../views/mycloum.vue')
      },
      {
        path: '/add',
        name: 'add',
        component: () => import('../views/addavter.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to,from,next) => {
  if(sessionStorage.getItem('token')){
   next()
  }else{
    if (to.path=='/add'||to.path=='/myList') {
      next('/login')
      store.commit("changEnter", false);
    }else{
      next()
    }
  }
})
export default router
