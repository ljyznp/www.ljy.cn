//国外疫情地图数据(不包括中国)-！！！注意要把数据单独处理下-累计确诊和现存确诊单独存储，因为有tab切换
export function setForeignData(foreignList){
	let confirmData = []; //累计确诊
	let nowConfirmData = [];//现在确诊
	let foreignData = {};
	for(let value of foreignList){
		confirmData.push({name:value.name,value:value.confirm})
		nowConfirmData.push({name:value.name,value:value.nowConfirm})
	}
	// foreignData = {confirmData:confirmData,nowConfirmData:nowConfirmData} //外面调用 foreignData.confirmData
	//简写
	foreignData = {confirmData,nowConfirmData} //外面调用 foreignData.confirmData

	return foreignData
}