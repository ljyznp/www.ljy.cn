import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from "vue-router";
import layout from '@/views/index'
const routes = [
  // 登陆
  //以下为测试页面
  //根目录,到登录页面
  //找不到页面到404,！！注意vue3里不能用*
  // {
  //  path: '/:catchAll(.*)',
  //  redirect: '/404'
  // }
  {
    path:'/',
    name:'index',
    component:layout
  }
];

const router = createRouter({
  history: createWebHashHistory(), // hash模式：createWebHashHistory，history模式：createWebHistory
  routes,
});

export default router;
