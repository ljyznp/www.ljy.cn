import { createStore } from "vuex";
import user from './modules/user'
import permission from './modules/permission'
import createPersistedState from 'vuex-persistedstate'
export default createStore({
  plugins:[createPersistedState()],
  state: {
    
  },
  mutations: {
   
  },
  actions: {
    
  },
  modules: {
    user,
    permission
  },
});
