import { constantRoutes, asyncRoutes } from "../../router";
const state = {
  routes: [],
};
const mutations = {
  // 存静
  setconstRoute(state) {
    state.routes = [...constantRoutes];
    // console.log(state.routes);
  },
  // 存动
  setRoutes(state, asyncRoutesList) {
    state.routes = [...constantRoutes, ...asyncRoutesList];
    console.log(state.routes, "state.routes");
  },
};
const actions = {
  filterRoutes(context, menusList) {
  return  new Promise((resolve, reject) => {
    const newRoutes = [];
    menusList.forEach((key) => {
      newRoutes.push(...asyncRoutes.filter((item) => item.name === key));
    });
    context.commit("setRoutes", newRoutes);
    resolve(newRoutes);
    })
    // // console.log(menusList);
    // const newRoutes = [];
    // menusList.forEach((key) => {
    //   newRoutes.push(...asyncRoutes.filter((item) => item.name === key));
    // });
    // context.commit("setRoutes", newRoutes);
    // return newRoutes;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
