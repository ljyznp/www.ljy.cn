import Cookies from "js-cookie";
import {getuserInfo ,getuserDetail} from '@/http/api'
const state = {
  token: "",
  userInfo:""
};
const mutations = {
  setToken(state, token) {
    state.token = token;
    Cookies.set("token", state.token);
    // console.log('cookie存储成功');
  },
  setuserInfo(state, userInfo) {
    state.userInfo=userInfo
    // console.log(state.userInfo);
  }
};
const actions = {
  setuserToken({ commit }, val) {
    commit("setToken", val.data.data);
  },
 async getInfo({ commit},val){
    let result =null
   await getuserInfo().then(res=>{
      console.log(res.data.data);
       result=res.data.data
    })
    console.log(result.roles.menus);
    let info=null
    getuserDetail(result.userId).then(res=>{
      // console.log(res);
      info=res.data.data
    });
    commit('setuserInfo', {...result,...info});
    return result.roles.menus;

  }
};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
