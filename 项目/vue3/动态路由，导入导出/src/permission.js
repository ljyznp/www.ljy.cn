import router from '@/router'
import store from '@/store'
const whiteList = ["/login"];
router.beforeEach(async (to, from, next) => {
  if (Cookies.get("token")) {
    if (!store.state.userInfo) {
      let menus = await store.dispatch("user/getInfo");
      let newRoute = store.dispatch("permission/filterRoutes", menus);
      console.log(newRoute);
      next();
    } else {
      next();
    }
  } else {
    if (whiteList.includes(to.path)) {
      next();
    } else {
      next("/login");
    }
  }
});

