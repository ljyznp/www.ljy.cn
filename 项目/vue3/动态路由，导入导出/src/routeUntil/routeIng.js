export function tranRoute(data) {
let arr=[]
function deepRoute(list){
    list.forEach(item => {
        if (item.children&&item.children.length) {
            deepRoute(item.children);
        }else{
            let baseRouterUrl=''
            if (!item.name) {
                baseRouterUrl=item.path
                // console.log(baseRouterUrl);
            }else{
              baseRouterUrl=item.name
            //   console.log(baseRouterUrl);
            }
            let routerObj={
                path:'/'+baseRouterUrl,
                name:item.name,
                component: item.component,
            }
            // console.log(routerObj);
            arr.push(routerObj);
        }
        // console.log(arr, 'Array');
      });
}
deepRoute(data)
return arr
}
