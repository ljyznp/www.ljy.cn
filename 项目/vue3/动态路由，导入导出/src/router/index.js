import {
  createRouter,
  createWebHashHistory,
  createWebHistory,
} from "vue-router";
import {tranRoute} from "../routeUntil/routeIng"
import Layout from "../views/index.vue";
import store from "../store";
import Cookies from "js-cookie";
import approvalsRouter from "./modules/approvals";
import departmentsRouter from "./modules/departments";
import employeesRouter from "./modules/employees";
import permissionRouter from "./modules/permission";
import attendancesRouter from "./modules/attendances";
import salarysRouter from "./modules/salarys";
import settingRouter from "./modules/setting";
import socialRouter from "./modules/social";
import userRouter from "./modules/user";
export let asyncRoutes = [
  approvalsRouter,    //approvalsRouter
  departmentsRouter,
  employeesRouter,
  permissionRouter,
  attendancesRouter,
  salarysRouter,
  settingRouter,
  socialRouter,
  userRouter,
];
export let constantRoutes = [
  // 登陆
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
  },
  {
    path: '/',
    name:'Home',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
];

export const router = createRouter({
  history: createWebHashHistory(), 
 
  // routes: [...constantRoutes],
  routes: [...constantRoutes,...asyncRoutes],
});

const whiteList = ["/login"];
router.beforeEach(async (to, from, next) => {
  if (Cookies.get("token")) {
    if (to.path=='/login') {
      next('/')
    } else{
      if (!store.state.userInfo) {
        let menusList = await store.dispatch("user/getInfo");
        store.dispatch("permission/filterRoutes", menusList).then(res=>{
          tranRoute(res)
          console.log(tranRoute(res));
          tranRoute(res).forEach(item=>{
            router.addRoute('Home',item)
          })
        });
        next();
      } else {
        next();
      }
    }
   
   
  } else {
    if (whiteList.includes(to.path)) {
      next();
    } else {
      next("/login");
    }
  }
});

export default router;
