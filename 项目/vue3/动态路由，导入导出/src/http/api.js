// 引入二次封装的 axios
import app from "./request";

// 登录
export async function getLogin(msg) {
  let data = await app.post("sys/login", msg);
  return data;
}
export async function getuserInfo() {
  let data = await app.post("/sys/profile");
  return data;
}
export async function getuserDetail(id) {
  let data = await app.get(`/sys/user/${id}`);
  return data;
}
export async function getEmployeeList(params) {
  let data = await app.get('/sys/user',{params});
  return data;
}
export async function importEmployee(data) {
  let list = await app({
   url: '/sys/user/batch',
   method: 'post',
   data,
   headers: {
    "Content-Type": "multipart/form-data",//修改请求头为formData格式
  },
  });
  return list;
}

