// pages/requests/requests.js
import {getbanner, getnav,getone} from '../../http/http'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title:'搜索',
    banner:[],  //轮播图
    nav:[],  //导航
    one:[],  //楼层
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
   //请求数据  轮播图
   getbanner().then(res=>{
    console.log(res);
    this.data.banner=res.data.message
    //响应式
    this.setData({
      banner:this.data.banner
    })
  })

  //导航
  getnav().then(res=>{
    console.log(res);
    this.data.nav=res.data.message
    //响应式
    this.setData({
      nav:this.data.nav
    })

    //楼层
    getone().then(res=>{
      console.log(res);
      this.data.one=res.data.message
      //响应式
      this.setData({
        one:this.data.one
      })
      console.log(this.data.one,'floor');
    })
  })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})