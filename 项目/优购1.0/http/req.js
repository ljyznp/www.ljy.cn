//封装promise对象
//基准地址
const baseUrl="https://api-hmugo-web.itheima.net/api/public/v1"
// https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata
//配对使用 async+await 小程序中复杂 
const request=(params)=>{
  return new Promise((resolve,reject)=>{
    wx.request({
      ...params,
      url: baseUrl+params.url,
      // method:'get', 默认get请求
      success:(res)=>{
        resolve(res)
      },
      fail:(err)=>{
        reject(err)
      }
    })
  })
}

export default request  