//引入
import request from './req'

//请求轮播图
function getbanner(){
  return request({url:'/home/swiperdata'})
}

//导航
function getnav(){
  return request({url:'/home/catitems'})
}
//楼层
function getone(){
  return request({url:'/home/floordata'})
}
//分类
function getCate(){
  return request({url:'/categories'})
}
//导出
export {getnav,getone,getCate}
