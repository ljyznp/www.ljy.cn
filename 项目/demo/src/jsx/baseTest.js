class Person{
    constructor(name,age,sex){
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
    fn(){
        console.log(this.name,this.age,this.sex);
    }
}
const son=new Person('LiHua','18','男');
console.log(son);
son.fn()
class grandSon extends Person{
    constructor(name,age,sex){
        super()
        this.name = name;
        this.age = age;
        this.sex = sex;
    }
    render(){
        return (
            <a href="www.baidu.com">百度</a>
        )
    }
    fn1(){
        console.log(this);
    }
}
const people=new grandSon('tom','18','未知')
console.log(people);
people.fn1()
