import "./App.css";
import React from "react";
import GtProps from "./组件通信/functionProps";
import { ClassApp } from "./组件通信/classProps";
// import { Print } from "./组件定义/garlrey";
// import { Hello } from "./组件定义/firstClass";
// import './jsx/baseTest'
// import {People} from "./组件定义/stateClass";
// import Num from './受控组件/myFrom'
// const songs = [
//   { name: "青花瓷", id: 1 },
//   { name: "你还要我怎样", id: 2 },
//   { name: "刚刚好", id: 3 },
// ];
// const list = (
//   <ul className="title" style={{ color: "red" }}>
//     {songs.map(function (song) {
//       return <li key={song.id}>{song.name}</li>;
//     })}
//   </ul>
// );
class Playlist extends React.Component {
  constructor() {
    super();
    this.textRef = React.createRef();
    this.state = {
      list: [
        { id: 1, name: "轿车", price: "10万" },
        { id: 2, name: "轿车", price: "12万" },
        { id: 3, name: "面包车", price: "15万" },
        { id: 4, name: "卡车", price: "12.5万" },
        { id: 5, name: "运钞车", price: "15.4万" },
        { id: 6, name: "冲锋车", price: "16.7万" },
      ],
      text: "请输入",
    };
  }
  handleDel = (item, index, id) => {
    console.log(item, index, id);
    this.setState({
      list: this.state.list.filter((item) => {
        return item.id !== id;
      }),
    });
  };
  handlePush = (e) => {
    this.setState({ text: e.target.value });
    console.log(this.textRef.current.value);
  };
  handlePushList = (e) => {
    if (e.keyCode === 13) {
      let list = this.state.list;
      list.push({
        id: new Date().getTime(),
        name: e.target.value,
        price: "13万",
      });
      console.log(list);
      this.setState({
        list: list,
        text: "",
      });
    }
  };
  render() {
    return (
      <div>
        <input
          ref={this.textRef}
          value={this.state.text}
          className="input"
          onChange={this.handlePush}
          onKeyDown={this.handlePushList}
          onFocus={() => {
            this.setState({
              text: "",
            });
          }}
        />
        <table className="tables">
          <thead>
            <tr>
              <th>编号</th>
              <th>名称</th>
              <th>价格</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            {this.state.list.map((item, index) => {
              return (
                <tr key={item.id}>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td
                    onClick={(e) => {
                      this.handleDel(item, index, item.id);
                    }}
                  >
                    删除
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

function App() {
  return (
    <div className="App">
      {<Playlist></Playlist>}
      {<GtProps name='jack'></GtProps>}
      {<ClassApp name="jack"></ClassApp>}
      {/* {list} */}
      {/* {<Print></Print>}
      {<Hello></Hello>}
      {<People></People>}
      {<Num></Num>} */}
    </div>
  );
}

export default App;
