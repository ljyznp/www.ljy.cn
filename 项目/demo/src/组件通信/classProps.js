import React from "react";
class ClassApp extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            num:1
        }
    }
    render(){
        return(
            <h1>名称{this.props.name}</h1>
        )
    }
}
export {ClassApp}