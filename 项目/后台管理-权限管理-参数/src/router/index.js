import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
import { tree } from "@/routeUntil/routeIng";
Vue.use(VueRouter);

const routes = [
  {
    path: "/home",
    name: "home",
    redirect: "/Welcome",
    component: HomeView,
    children: [
      { //欢迎页
        path: "/Welcome",
        name: "Welcome",
        component: () => import("../views/Welcome.vue"),
        meta: {
          title: "Welcome",
          frisTitle: "Welcome",
          secondTitle: "Welcome",
        },
      },


      // {  // 用户页
      //   path: "/users",
      //   name: "users",
      //   component: () => import("../views/./users/users.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "用户管理",
      //     secondTitle: "用户列表",
      //   },
      // },
      // {  // 用户页
      //   path: "/userstable",
      //   name: "users",
      //   component: () => import("../views/./users/user组件版.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "用户管理",
      //     secondTitle: "用户列表",
      //   },
      // },



      // {  //角色页
      //   path: "/roles",
      //   name: "roles",
      //   component: () => import("../views/./roles/roles.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "权限管理",
      //     secondTitle: "角色列表",
      //   },
      // },
      // {  //权限页
      //   path: "/rights",
      //   name: "rights",
      //   component: () => import("../views/./roles/rights.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "权限管理",
      //     secondTitle: "权限列表",
      //   },
      // },


      // {    //商品列表
      //   path: "/goods",
      //   name: "goods",
      //   component: () => import("../views/./goods/goods.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "商品管理",
      //     secondTitle: "商品列表",
      //   },
      // },
      // {    //商品添加页
      //   path: "/add",
      //   name: "add",
      //   component: () => import("../views/./goods/add.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "商品管理",
      //     secondTitle: "添加商品",
      //   },
      // },{    //商品分类页
      //   path: "/categories",
      //   name: "categories",
      //   component: () => import("../views/./goods/categories.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "商品管理",
      //     secondTitle: "商品分类",
      //   },
      // },
      // {    //商品参数页
      //   path: "/params",
      //   name: "params",
      //   component: () => import("../views/./goods/params.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "商品管理",
      //     secondTitle: "分类参数",
      //   },
      // },



      // {   // 数据
      //   path: "/reports",
      //   name: "reports",
      //   component: () => import("../views/./reports/reports.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "数据统计",
      //     secondTitle: "数据报表",
      //   },
      // },


      // {    //订单
      //   path: "/orders",
      //   name: "orders",
      //   component: () => import("../views/./orders/orders.vue"),
      //   meta: {
      //     title: "首页",
      //     frisTitle: "订单管理",
      //     secondTitle: "订单列表",
      //   },
      // },
    ],
  },
  // 登录页
  {
    path: "/login",
    name: "login",
    component: () => import("../views/login.vue"),
  },
];

const router = new VueRouter({
  routes,
});

function useTree() {
  let token=sessionStorage.getItem("token");
  let menus = JSON.parse(localStorage.getItem('menus'))
  console.log(token, menus)
  if (token&& menus) {
    let reg = tree(menus);
        console.log(reg);
        reg.forEach(item => {
          console.log(item);
          router.addRoute("home", item)
          console.log(router)
        })
  }
}
useTree()
router.beforeEach((to, from, next) => {
  if (sessionStorage.getItem("token")) {
    next();
  } else {
    if (to.path == "/login") {
      next();
    } else {
      next("/login");
    }
  }
});
export default router;
