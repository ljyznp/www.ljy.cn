export function tree(data) {
  let arr = []
  function deep(data) {
      data.forEach(item => {
          if (item.children.length) {
              //递归开始
              deep(item.children)
          } else {
             
              arr.push({
                  path: "/" + item.path,
                  name: item.path,
                  component: () => import(`../views/${item.path}.vue`)
              })
          }
      })
  }
  deep(data)
  return arr
}
