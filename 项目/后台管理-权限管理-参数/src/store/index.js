import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import { Tologin } from "@/api/common";
export default new Vuex.Store({
  state: {
    token: "",
  },
  getters: {},
  mutations: {
    setLogin(state, token) {
      state.token = token;
      sessionStorage.setItem("token", state.token);
      console.log(state.token);
    },
  },
  actions: {
    goLogin({ commit }, info) {
      return new Promise(function (resolve, reject) {
        // console.log(info);
        Tologin(info).then((res) => {
          // console.log(res.data.data.token);
          commit("setLogin", res.data.data.token);
          resolve(res);
        });
      });
    },
  },
  modules: {},
});
