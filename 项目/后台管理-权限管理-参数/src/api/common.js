import app from "./http";
// 登录验证接口
async function Tologin(params) {
  let data = await app.post("login", params);
  return data;
}
// 侧边
async function getMenus() {
  let data = await app.get("menus");
  return data;
}
// 用户数据列表
async function getusers(params) {
  let data = await app.get("users", { params });
  return data;
}
//  修改用户状态
async function getBool(params) {
  let data = await app.put(`users/${params.id}/state/${params.bool}`);
  return data;
}
// 删除单个用户
async function getDel(params) {
  let data = await app.delete(`users/${params.id}`);
  return data;
}
// 添加用户
async function getAdd(params) {
  let data = await app.post(`users`, params);
  return data;
}
// 编辑用户提交
async function getedit(params) {
  let data = await app.put(`users/${params.id}`, params);
  return data;
}
// 分配用户角色
async function getsaveRoles(params) {
  let data = await app.put(`users/${params.id}/role`, { rid: params.rid });
  return data;
}
// 角色数据列表
async function getroles() {
  let data = await app.get(`roles`);
  return data;
}
// 删除角色
async function delRoles(params) {
  console.log(params);
  let data = await app.delete(`roles/${params.id}`);
  return data;
}
// 添加角色
async function addRoles(params) {
  console.log(params);
  let data = await app.post(`roles/`, params);
  return data;
}
// 编辑提交角色
async function editRoles(params) {
  console.log(params, "000");
  let data = await app.put(`roles/${params.id}`, {
    roleName: params.roleName,
    roleDesc: params.roleDesc,
  });
  return data;
}
// 权限树
async function getRights(params) {
  let data = await app.get(`rights/tree`);
  return data;
}
// 权限列表
async function getRightstree(params) {
  let data = await app.get(`rights/list`);
  return data;
}
//  角色授权
async function saveRights({ roleId, rids }) {
  console.log(roleId, rids);
  let data = await app.post(`roles/${roleId}/rights`, { rids: rids.join(",") });
  return data;
}
// 权限列表
async function getRightslist(params) {
  let data = await app.get(`rights/list`);
  return data;
}
// 删除角色权限
async function delPower(params) {
  let data = await app.delete(`roles/${params.rid}/rights/${params.pid}`);
  return data;
}
// 商品列表数据
async function getGoods(params) {
  let data = await app.get(`goods`, { params });
  return data;
}
// 添加商品
async function addGoods(params) {
  let data = await app.post(`goods`, params);
  return data;
}
// 删除商品
async function delGoods({ id }) {
  let data = await app.get(`goods/${id}`);
  return data;
}

// categories分类
async function getCategories(params) {
  let data = await app.get(`categories`, { params });
  return data;
}
// categories删除分类
async function delCategories({ id }) {
  let data = await app.delete(`categories/${id}`);
  return data;
}
// categories编辑分类
async function EDitCategories({ id, cat_name }) {
  let data = await app.put(`categories/${id}`, { cat_name });
  return data;
}
// 添加分类
async function addCategories(params) {
  let data = await app.post(`categories`, params);
  return data;
}
// 订单数据列表
async function getorders(params) {
  let data = await app.get(`orders`, { params });
  return data;
}
// 查看物流信息
async function checkAddress(id) {
  let data = await app.get(`/kuaidi/${id}`);
  return data;
}
// 获取数据列表
async function getNumber() {
  let data = await app.get(`reports/type/1`);
  return data;
}
// 参数列表
async function getCATEParams(msg) {
  let data = await app.get(`categories/${msg.id}/attributes`,{ params: { sel: msg.sel } });
  return data;
}
// 删除参数
async function delarams({id,attrid}) {
  let data = await app.delete(`categories/${id}/attributes/${attrid}`);
  return data;
}
// 添加动态参数或者静态属性
async function addparams(params) {
  let data = await app.post(`categories/${params.id}/attributes`,{attr_name:params.attrname,attr_sel:params.attrsel,attr_vals:params.attrvals});
  return data;
}
// 编辑提交参数
async function editparams(params) {
  let data = await app.put(`categories/${params.id}/attributes/${params.attrid}`,{attr_name:params.attrname,attr_sel:params.attrsel,attr_vals:params.attrvals});
  return data;
}
export {
  Tologin,
  getMenus,
  getusers,
  getBool,
  getDel,
  getAdd,
  getedit,
  getroles,
  getsaveRoles,
  delRoles,
  addRoles,
  editRoles,
  getRights,
  saveRights,
  getRightslist,
  delPower,
  getNumber,
  getorders,
  checkAddress,
  getGoods,
  delGoods,
  addGoods,
  getRightstree,
  getCategories,
  delCategories,
  EDitCategories,
  addCategories,
  getCATEParams,
  delarams,
  addparams,
  editparams
};
