// 引入axios
import axios from "axios";
// 引入element-ui实例
import { MessageBox, Message, Loading } from "element-ui";
import router from "@/router";
// 创建实例
const app = axios.create({
  // 基准地址
  baseURL: "https://www.liulongbin.top:8888/api/private/v1/",
  // baseURL: process.env.VUE_APP_API, //根据不同环境应用不同接口
  // // 超时时间
  timeout: 1000,
});
// 添加请求拦截器
var loading; //loading加载
app.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    //  配置token
    //  loading加载
    loading = Loading.service({
      lock: true,
      text: "Loading",
      icon: "iconfont icon-loading",
      background: "rgba(0, 0, 0, 0.7)",
    });
    config.headers.Authorization = sessionStorage.getItem("token");

    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
app.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    loading.close(); //响应成功后关闭loading
    let msg = response.data.meta.msg; //判断是否是无效token的依据
    let status = response.data.meta.status; //响应状态码
    // 处理无效token
    if (msg == "无效token") {
      MessageBox.confirm("token过期是否留在本页面?", "提示", {
        confirmButtonText: "确定",
        cancelButtonText: "取消",
        type: "warning",
      })
        .then(() => {
          router.push("/login");
        })
        .catch(() => {
          return false;
        });
    }
    // 处理状态码
    if (
      status === 400 ||
      status === 401 ||
      status === 403 ||
      status === 404 ||
      status === 401 ||
      status === 403 ||
      status === 500
    ) {
      Message({
        message: status,
        type: "error",
      });
    } else {
      Message({
        message: msg,
        type: "success",
      });
    }
    // console.log(msg, status);
    // console.log(response.data.meta, 'response');
    return response;
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);
export default app;
