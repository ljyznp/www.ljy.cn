const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  //配置输出路径，没有设置会导致白屏，因为输出到index.html里的是根目录
  publicPath: './',
  transpileDependencies: true,
  // chainWebpack 通过链式编程的形式来修改默认的webpack配置
  chainWebpack:config=>{
    config.when(process.env.NODE_ENV === 'production',config=>{
      // 从哪里开始编译打包，根据入口文件依赖关系进行打包编译
      config.entry('app').clear().add('./src/main.js')
      // 抽离
      config.set('externals',{
        // 'vue':'Vue',
        'echarts':'echarts',
        // 'element-ui':'ELEMENT'
      })
      config.plugin('html').tap(args=>{
        return args
      })
    })
    config.when(process.env.NODE_ENV==='development',config=>{
      config.entry('app').clear().add('./src/main.js')
      config.plugin('html').tap(args=>{
        return args
      })
    })
  }
})
