export function http(params) {
  // 基准地址
  let baseUrl = 'https://api-hmugo-web.itheima.net/api/public/v1'
  // 提示信息loading
  wx.showLoading({
    title: '加载中...',
  })
  return new Promise((resolve,reject)=>{
    wx.request({
      ...params,//请求信息
      url: baseUrl+params.url,//地址,
      header:{
        Authorization:wx.getStorageSync('token')
      },
      // 成功
      success:(res)=>{
        resolve(res)
      },
      // 错误
      fail:(error)=>{
        reject(error)
      },
      // 无论成功还是失败，关闭loading
      complete:()=>{
      wx.hideLoading({
        success: (res) => {
          // console.log(res);
        },
      })
      }
    })
  })
}