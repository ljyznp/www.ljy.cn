import {http} from './http'
export async function getSwiper (){
  let data=await http({url:'/home/swiperdata',method:'GET'})
  return data
}
//导航
function getnav(){
  return http({url:'/home/catitems'})
}
//楼层
function getone(){
  return http({url:'/home/floordata'})
}
//分类
function getCate(){
  return http({url:'/categories'})
}
// 获取商品列表
function getGoodsList(params){
    return http({url:'/goods/search',data:params})
  }
  // 获取商品详情
function getGoodsDetail(params){
    return http({url:'/goods/detail',data:params})
  }
    // 获取订单
function getGoodsorder(params){
    return http({url:'/my/orders/req_unifiedorder',data:params})
  }
//   创建订单
  function createGoodsorder(params){
    return http({url:'/my/orders/create',data:params,method:'post'})
  }
//   订单查询
function searchOrder(params){
    return http({url:'/my/orders/all',data:params})
}
//导出
export {getnav,getone,getCate,getGoodsList,getGoodsDetail,getGoodsorder,createGoodsorder,searchOrder}