// pages/pay/pay.js
import {createGoodsorder} from '../../common/api'
import {http} from '../../common/http'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        address: wx.getStorageSync('address'),
        cart: wx.getStorageSync('shopcar'),
        allPrices: 0
    },
    onSubmit() {
        let myCart = wx.getStorageSync('shopcar')
        let myAddress = wx.getStorageSync('address')
        console.log(myCart,myAddress);
        let num=0
        myCart.forEach(item=>{
                num=item.num*item.goods_price
                console.log(num);
        })
        this.data.allPrices=num
        let arr=[];
        myCart.forEach(item=>{
            arr.push({"goods_id":item.goods_id,"goods_number":Number(item.num),"goods_price":item.goods_price}) 
        })
         console.log(wx.getStorageSync('token'));
         if (wx.getStorageSync('token')=='') {
            wx.switchTab({
              url: '/pages/user/user',
            })
         }
         console.log(num,this.data.address.localAddress,arr);
         let obj={
            order_price	:this.data.allPrices+'',
            consignee_addr: this.data.address.localAddress.toString(),
            goods:arr
         }
         console.log(obj);
        createGoodsorder(obj).then(async(res)=>{
            console.log(res,'order');
            if(res.data.meta.status===200){
                let order_number=res.data.message.order_number
                let res1=await http({url:'my/orders/req_unifiedorder',method:'post',data:{"order_number":order_number}})
                console.log('预支付订单',res1);
                if(res1.data.meta.status===200){
                    let {pay}=res1.message
                    console.log('pay',pay);
                    //4.拉起微信的支付窗口
                    wx.requestPayment({
                      ...pay,
                      success (res) { 
                        console.log('拉起成功',res);  //如果有权限是能拉起成功的
                      },
                      fail (err) {
                        console.log('拉起失败',err); //咱账号没有权限，所以会拉起失败 {errMsg: "requestPayment:fail no permission"}
                       }
                    })
      
                }
                
                //5.查询订单支付状态，提醒用户是否支付成功
                let res2=await http({url:'my/orders/chkOrder',method:'post',data:{"order_number":order_number}})
                console.log('是否支付',res2);
                if(res2.data.meta.status===200){
                   wx.showToast({
                     title: '支付成功',
                   })
                   wx.navigateTo({
                     url: '/pages/order/order',
                   })
                } 
                else{
                  wx.showToast({
                    title: '支付失败',
                  })
                } 
      
           }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        console.log(wx.getStorageSync('shopcar'),wx.getStorageSync('address'));
        this.data.address=wx.getStorageSync('address')
        this.data.cart=wx.getStorageSync('shopcar')
        let num=0
        this.data.cart.forEach(item=>{
                num=item.num*item.goods_price
                console.log(num);
        })
        this.data.allPrices=num
        this.setData({
            address: this.data.address,
            cart: this.data.cart,
            allPrices: this.data.allPrices
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        console.log(wx.getStorageSync('shopcar'),wx.getStorageSync('address'));
        this.data.address=wx.getStorageSync('address')
        this.data.cart=wx.getStorageSync('shopcar')
        let num=0
        this.data.cart.forEach(item=>{
                num=item.num*item.goods_price
                console.log(num);
        })
        this.data.allPrices=num
        this.setData({
            address: this.data.address,
            cart: this.data.cart,
            allPrices: this.data.allPrices
        })

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})