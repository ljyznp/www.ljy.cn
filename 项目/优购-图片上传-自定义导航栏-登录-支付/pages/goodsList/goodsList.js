// pages/goodsList/goodsList.js
import {
    getGoodsList
} from '../../common/api'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        //接口请求参数
        QueryParams: {
            query: "",
            cid: "",
            pagenum: 1,
            pagesize: 10
        },
        ind: 1,
        goodslist: [],
        baselist: [],
        total: ''
    },
    tabChange(e) {
        // console.log(e.currentTarget.dataset.num);
        this.ind = e.currentTarget.dataset.num
        console.log(this.ind);
        if (this.ind == 3) {
            this.goodslist = this.baselist.sort((a, b) => {
                return a.goods_price - b.goods_price
            })
        }
        this.setData({
            ind: this.ind,
            goodslist: this.goodslist
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        console.log(options, 'cid');
        // this.data.QueryParams.query=options.name
        this.data.QueryParams.cid=options.cid||''
        getGoodsList(this.data.QueryParams).then(res => {
            console.log(res);
            this.goodslist = res.data.message.goods
            this.baselist = res.data.message.goods
            this.total = res.data.message.total
            this.setData({
                goodslist: this.goodslist,
                total: this.total,
                baselist: this.baselist
            })
        })
    },
    Todetail(e){
    console.log(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: `/pages/goods_detail/index?goods_id=${e.currentTarget.dataset.id}`,
    })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        this.data.QueryParams.pagenum=1
     console.log(this.data.QueryParams);
     getGoodsList(this.data.QueryParams).then(res=>{
       console.log(res.data.message.goods);
       this.data.goodslist=res.data.message.goods
       console.log(this.data.goodslist);
     })
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        console.log(this.data.goodslist);
        this.data.QueryParams.pagenum = this.data.QueryParams.pagenum + 1
        console.log(this.data.QueryParams.pagenum);
        getGoodsList(this.data.QueryParams).then(res => {
            console.log(res);
            console.log(this.data.goodslist);
            this.data.goodslist = [...this.data.goodslist,...res.data.message.goods]
            this.data.baselist = [...this.data.baselist,...res.data.message.goods]
            this.data.total = res.data.message.total
            console.log(this.data.goodslist,this.data.baselist,this.data.total);
            this.setData({
                goodslist: this.data.goodslist,
                total: this.data.total,
                baselist: this.data.baselist
            })
        })
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})