// pages/car/car.js
import {
    getGoodsorder
} from '../../common/api'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        goodsList: wx.getStorageSync('carList')||[],  //商品
        isShow: false, //地址栏
        address: {
            name: '张三',
            phone: '020-81167888',
            localAddress: '广东省广州市海珠区新港中路397号'
        },//地址
        allSelect: false,//全选
        allPrices: 0//总价
    },

    getAddress() {
        console.log(this.data);
        wx.setStorageSync('address', this.data.address)
        this.setData({
            isShow: true
        })
    },
    onClickButton(){
        wx.navigateTo({
          url: '/pages/pay/pay',
        })
    },
    handelselect(e) {
        // 获取索引
        let index = e.currentTarget.dataset.index
        // 改变状态
        this.data.goodsList[index].status = !this.data.goodsList[index].status
        console.log(this.data.goodsList);
        wx.setStorageSync('carList', this.data.goodsList)
        // 获取选中
        let ArrayLength = this.data.goodsList.filter(item => {
            return item.status == true
        }).length
        let shopcar=this.data.goodsList.filter(item => {
            return item.status == true
        })
        console.log(shopcar);
        wx.setStorageSync('shopcar', shopcar)
        // 全选
        if (ArrayLength == this.data.goodsList.length) {
            this.data.allSelect = true
        } else {
            this.data.allSelect = false
        }
        // 计算总价
        let num = 0
        this.data.goodsList.filter(item => {
            return item.status == true
        }).forEach(item => {
            num = item.num * item.goods_price
            console.log(num);
        })
        // 赋值
        this.data.allPrices = num
        // 响应
        this.setData({
            allSelect: this.data.allSelect,//全选状态
            allPrices: this.data.allPrices //总价
        })
    },
    handleAll(){
      this.data.allSelect=!this.data.allSelect
      console.log(this.data.allSelect);
      this.data.goodsList.forEach(item=>{
        item.status=this.data.allSelect
      })
      console.log(this.data.goodsList);
      this.setData({
        goodsList :this.data.goodsList
      })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
            goodsList: wx.getStorageSync('carList') || [],//所有商品
            address: this.data.address,//地址
            allSelect: this.data.allSelect,//全选状态
            allPrices: this.data.allPrices//总价
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        this.setData({
            goodsList: wx.getStorageSync('carList') || [],//所有商品
            address: this.data.address,//地址
            allSelect: this.data.allSelect,//全选状态
            allPrices: this.data.allPrices//总价
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})