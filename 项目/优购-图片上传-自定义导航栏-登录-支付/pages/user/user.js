// pages/user/user.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  goOrder(){
    wx.navigateTo({
      url: '/pages/order/order',
    })
  },
  upload(){
    wx.navigateTo({
      url: '/pages/upload/upload',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    const userinfo=wx.getStorageSync("userinfo");
    this.setData({userinfo});
  },
  goout(){
  wx.showModal({
      title:'是否退出',
      success:(res)=>{
        wx.removeStorageSync('token')
        wx.removeStorageSync('userinfo')
        // 关闭所有页面，打开到应用内的某个页面-相当于刷新
        wx.reLaunch({
          url: '/pages/index/index'
        })
      }
  })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})