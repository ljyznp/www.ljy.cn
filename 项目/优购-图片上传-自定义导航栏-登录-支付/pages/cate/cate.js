// pages/cate/cate.js
import {getCate} from '../../common/api'
Page({

    /**
     * 页面的初始数据
     */
    data: {
    cateList:[],
    ind:0
    },
    handleClick(e){
      console.log(e);
      let index=e.currentTarget.dataset.index
      this.setData({
        ind:index
      })
      console.log(this.data.ind);
    },
    toList(e){
        console.log(e.currentTarget.dataset.cid);
        wx.navigateTo({
            url:`/pages/goodsList/goodsList?cid=${e.currentTarget.dataset.cid}`
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
      getCate().then(res=>{
        console.log(res,'cate');
       this.data.cateList=res.data.message
       console.log(res.data.message,'res.data.message');
       this.setData({
         cateList:this.data.cateList
       })
       console.log(this.data.cateList,'this.data.cateList');
      })
    },
  
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {
     
    
    },
  
    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
  
    },
  
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
  
    },
  
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
  
    },
  
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
  
    },
  
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
  
    },
  
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {
  
    }
  })