// pages/upload/upload.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    fileList: [], //图片存放的数组
  },


  // 删除照片
  deleteClick(event) {
    var imgData = this.data.fileList;
    // 通过splice方法删除图片
    imgData.splice(event.detail.index, 1);
    // 更新图片数组
    this.setData({
      fileList: imgData
    })
  },

  afterRead(event) {
    wx.showLoading({
        title: '上传中...'
    });
    const { file} = event.detail; //获取图片详细信息
    console.log(file);
    // 调用wx.uploadFile上传图片方法
    wx.uploadFile({
      url: "https://www.liulongbin.top:8888/api/private/v1/upload",
      method: 'POST',
      timeout:'2000',
      header: {
        Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjUwMCwicmlkIjowLCJpYXQiOjE2Nzk0NTIxODUsImV4cCI6MTY3OTUzODU4NX0.35S4um5wtpFcnOth5gFdrxvEOTumRqOUfS8dMwpy67Y'
    },
      filePath: file.url,
      name: 'file',
      // 成功回调
      success:(res)=> {
          console.log(res);
        // JSON.parse()方法是将JSON格式字符串转换为js对象
        var result = JSON.parse(res.data);
        // 上传完成需要更新 fileList
        const {fileList = []} = this.data;
        // 将图片信息添加到fileList数字中
        fileList.push({
            ...file,
            url: result.data
        });
        // 更新存放图片的数组
        this.setData({
            fileList
        });
        wx.hideLoading();//停止loading


      },
      fail:(error)=>{
        console.log(error);
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      fileList: this.data.fileList
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})