// pages/order/order.js
import {
    searchOrder
} from '../../common/api'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        params: {
            type: 1
        },
        orderList: [],

    },
    change(e) {
        console.log(e.currentTarget.dataset.ind);
        this.data.params.type = e.currentTarget.dataset.ind
        searchOrder(this.data.params).then(res => {
            console.log(res.data.message.orders);
            this.data.orderList = res.data.message.orders
            console.log(this.data.orderList);
        })
        this.setData({
            orderList: this.data.orderList,
            type: this.data.params.type
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        searchOrder(this.data.params).then(res => {
            console.log(res.data.message.orders);
            this.data.orderList = res.data.message.orders
            console.log(this.data.orderList);
            this.setData({
                orderList: this.data.orderList,
                type: this.data.params.type
            })
        })
    
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        searchOrder(this.data.params).then(res => {
            console.log(res.data.message.orders);
            this.data.orderList = res.data.message.orders
            console.log(this.data.orderList);
        })
        this.setData({
            orderList: this.data.orderList,
            type: this.data.params.type
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})