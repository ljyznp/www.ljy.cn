import {
    http
} from "../../common/http"
Page({
    data: {
        userInfo: ''
    },
    getUserProfile(e) {
        //1.通过开放能力，获取用户信息
        wx.getUserProfile({
            desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
                console.log('获取用户信息', res);
                wx.setStorageSync('userinfo', res.userInfo) //登录页面要用的头像信息
                let {
                    encryptedData,
                    iv,
                    rawData,
                    signature
                } = res //换到token时用的

                //2.通过开放能力wx.login登录，获取code验证码,5分钟有效期
                wx.login({
                    success(res1) {
                        let {
                            code
                        } = res1
                        if (res1.code) {
                            //发起网络请求
                            http({
                                url: 'users/wxlogin',
                                data: {
                                    encryptedData,
                                    iv,
                                    rawData,
                                    signature,
                                    code
                                },
                                method: 'post'
                            }).then(res2 => {
                                wx.setStorageSync('token', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjIzLCJpYXQiOjE1NjQ3MzAwNzksImV4cCI6MTAwMTU2NDczMDA3OH0.YPt-XeLnjV-_1ITaXGY2FhxmCe4NvXuRnRB8OMCfnPo')

                                //成功后返回上一页面
                                wx.navigateBack({
                                    delta: 1
                                })
                            })
                        } else {
                            wx.showToast({
                                title: '授权失败',
                            })
                        }
                    }
                })


            }
        })
    },

})