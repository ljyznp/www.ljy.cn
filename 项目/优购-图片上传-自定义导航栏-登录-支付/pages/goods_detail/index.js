// pages/goods_detail/index.js
import {getGoodsDetail} from '../../common/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsObj: {}, //接口返回数据
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
   console.log(options.goods_id);
   getGoodsDetail(options).then(res=>{
       console.log(res);
       this.goodsObj=res.data.message
       this.goodsObj.goods_introduce= this.goodsObj.goods_introduce.replace('/\.webp/g', '.jpg')
       console.log( this.goodsObj);
       this.setData({
           goodsObj:this.goodsObj
       })
   })
  },
  handleCartAdd(){
      let carList=wx.getStorageSync('carList')||[]
      let bool=true
      carList.forEach(item=>{
       if (item.goods_name==this.goodsObj.goods_name) {
           bool=false
           item.num++
       }
    })
    if (bool) {
      carList.push({...this.goodsObj,status:false,num:'1'})
    }
      console.log(carList);
      wx.showToast({
        title: '添加成功',
      })
      wx.setStorageSync('carList', carList)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})