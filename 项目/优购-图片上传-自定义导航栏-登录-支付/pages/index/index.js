// index.js
// 获取应用实例
const app = getApp()
import {getSwiper,getnav,getone,getCate} from '../../common/api'
Page({
  data: {
   SwiperList:[],//轮播图
   Nav:[],//导航
   floor:[] //楼层
  },
  TogoodsList(e){
    console.log(e.currentTarget.dataset.name);
    let NavName=e.currentTarget.dataset.name
    if (NavName=='分类') {
      wx.switchTab({
        url: '/pages/cate/cate',
      })
    }else{
        wx.navigateTo({
            url:`/pages/goodsList/goodsList?name=${NavName}`
        })
    }
  },
  onLoad() {
    // 获取轮播图
    getSwiper().then(res=>{
      // console.log(res.data.message);
      this.SwiperList=res.data.message
      // console.log(this.SwiperList);
      this.setData({
        SwiperList:this.SwiperList
      })
    })
    //  获取导航
    getnav().then(res=>{
      // console.log(res);
      this.Nav=res.data.message
      // console.log(this.Nav);
      this.setData({
        Nav:this.Nav 
      })
    })
    //获取楼层
    getone().then(res=>{
      console.log(res.data.message);
      this.floor=res.data.message
      console.log(this.floor);
      this.setData({
        floor:this.floor
      })
    })
  },

})
