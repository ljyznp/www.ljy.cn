export default {
	api:function(){
		// uni端判断生产环境或开发环境
		let HOST_H5 = 'https://m.mengxuegu.com/#/'
		
		if(process.env.NODE_ENV === 'development'){
		    console.log('开发环境')
			HOST_H5 = 'http://m.mengxuegu.com/api'
		}else{
		    console.log('生产环境')
			HOST_H5 = 'https://m.mengxuegu.com/#/'
		}
		
       return HOST_H5
	}
	
}
